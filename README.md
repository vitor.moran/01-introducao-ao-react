# Aula 01: Introdução ao GIT e React

## GIT

Git é um sistema de controle de versão de arquivos. Através deles podemos desenvolver projetos na qual diversas pessoas podem contribuir simultaneamente no mesmo, editando e criando novos arquivos e permitindo que os mesmos possam existir sem o risco de suas alterações serem sobrescritas.

Se não houver um sistema de versão, imagine o caos entre duas pessoas abrindo o mesmo arquivo ao mesmo tempo. Uma das aplicações do git é justamente essa, permitir que um arquivo possa ser editado ao mesmo tempo por pessoas diferentes. Por mais complexo que isso seja, ele tenta manter tudo em ordem para evitar problemas para nós desenvolvedores.

[Download Git](https://git-scm.com/downloads)

### Fork

Fork é uma cópia do repositório original em outro `namespace` onde é possível fazer alterações sem afetar o projeto original.

Para "forkar" um projeto no Gitlab, basta clicar em `Fork` na parte superior da página do projeto, e em seguida escolher o `namespace`.

### Clone

Clonar um projeto, é a mesma coisa que baixar um projeto em sua máquina. Diferente do Fork, alterações em um projeto clonado, afetam o projeto original.

Para clonar um projeto, clique em `Clone` na parte superior do projeto, e copie a opção `Clone with HTTPS`. Entre uma basta pelo terminal e execute o comando `git clone url-copiada`.

### Commit

Antes de falarmos de commits, precisamos falar fo `git add`: Este comando adiciona o(s) arquivo(s) em um lugar que chamamos de INDEX, que funciona como uma área do git no qual os arquivos possam ser enviados ao reposiório. É importante saber que ADD não está adicionando um arquivo novo ao repositório, mas sim dizendo que o arquivo (sendo novo ou não) está sendo preparado para entrar na próxima revisão do repositório.

Exemplo: 
- `git add .` - Adiciona todos os arquivos.
- `git add "index.html"` adiciona somente o arquivo desejado.

Agora vamos ao `commit`:

Este comando realiza o que chamamos de “commit”, que significa pegar todos os arquivos que estão naquele lugar INDEX que o comando `add` adicionou e criar uma revisão com um número e um comentário, que será vista por todos.

Exemplo:
- `git commit -m "comentário qualquer"`

Além disso, temos o `git push`: Push (empurrar) é usado para publicar todos os seus commits para o repositório. Neste momento, será pedido a sua senha:

Exemplo:
- `git push origin master`

Extra:
- `git status` exibe o status do seu repositório atual.

### Branchs
Suponha que o seu projeto seja um site html, e você deseja criar uma nova seção no seu código HTML, mas naquele momento você não deseja que estas alterações estejam disponíveis para mais ninguém, só para você. Isso é, você quer alterar o projeto (incluindo vários arquivos nele), mas ainda não quer que isso seja tratado como “oficial” para outras pessoas, então você cria um branch (como se fosse uma cópia espelho) e então trabalha apenas nesse branch, até acertar todos os detalhes dele.

No git, o conceito de branch é algo muito simples e fácil de usar. Mas quando que temos que criar um branch? Imagine que o seu site está pronto, tudo funcionando perfeitamente, mas surge a necessidade de alterar algumas partes dele como forma de melhorá-lo. Além disso, você precisa manter estas alterações tanto no computador de casa quanto do trabalho. Com isso temos um problema, se você começa a alterar os arquivos em casa, para na metade da implementação, e precisa terminar no trabalho, como você iria comitar tudo pela metade e deixar o site incompleto?

Para isso existe o conceito de branch, que é justamente ramificar o seu projeto em 2, como se cada um deles fosse um repositório, e depois juntá-lo novamente.

Use o seguinte comando de terminal para criar um branch:

`git checkout -b 'hotfix'`, onde hotfix é nome da branch nova.

Mudar de branch:

`git checkout master`, onde master é o nome da branch já existente.


### MRs
Os MRs (Merge requests) permitem que você troque as alterações feitas no código-fonte e colabore com outras pessoas no mesmo projeto.

Um MR é a base do GitLab como plataforma de colaboração de código e controle de versão. É tão simples quanto o nome implica: um pedido para mesclar(`merge`) um `branch` em outro.

Com os MRs do GitLab, você pode:

- Compare as mudanças entre duas branches.
- Revisar e discutir as modificações propostas em linha.
- Live preview das alterações.
- Impedir que o MR seja mesclado antes de estar pronta com os WIP MRs.
- Resolver conflitos de mesclagem da interface do usuário.
- Solicitar aprovações.
- Etc.

#### Docs:
- [Git](https://git-scm.com/)
- [Tudo que você queria saber sobre Git e GitHub, mas tinha vergonha de perguntar](https://tableless.com.br/tudo-que-voce-queria-saber-sobre-git-e-github-mas-tinha-vergonha-de-perguntar/)
- [Merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/)

## Introdução React

React é uma biblioteca JavaScript e, portanto, vamos supor que você tenha uma compreensão básica da linguagem JavaScript.

React é uma biblioteca JavaScript, eficiente e flexível para a criação de interfaces de usuário (UI), que surgiu em 2011, no Facebook.

Dizer que React é uma biblioteca significa que este não é um framework, mas simplesmente uma coleção de funcionalidades relacionadas que podem ser chamadas pelo desenvolvedor para resolver problemas específicos — a criação de interfaces de usuário reaproveitáveis, no caso do React.

Já a principal característica de um framework é a Inversão de Controle, também conhecida como Hollywood Principle. Ou seja, quem dita o que (e como) algo deve ser feito é o framework, não o desenvolvedor — é ele quem chama o seu código, e não o contrário.

O importante disso tudo é que, em geral, bibliotecas são mais flexíveis e menos complexas do que frameworks.

No caso do React, sua única e principal função é a criação de interfaces de usuário, que organiza o que será mostrado para o usuário final na tela sem se preocupar em saber sobre o resto.

## React "create-react-app"

O JSX é incrível, mas precisa ser transpilado para JavaScript regular antes de chegar ao navegador. Normalmente, usamos um transpiler como o Babel para fazer isso. Podemos executar o Babel com uma ferramenta de build como o Webpack, que ajuda a agrupar todos nossos assets (arquivos de JavaScript, CSS, imagens, etc.) para projetos na web.

Para simplificar essas configurações iniciais, podemos usar o pacote do aplicativo Create React, do Facebook, para cuidar de toda a configuração para nós! Essa ferramenta é incrivelmente útil para iniciar a criação de um aplicativo em React, uma vez que configura tudo de que precisamos com nenhuma configuração manual! Instale o aplicativo Create React (na linha de comando, com `npm`), e com isso poderemos testar e ver o que o torna tão bom de ser utilizado.

`npm install -g create-react-app`

### Comandos:

- `create-react-app my-app`, onde `my-app` será o nome do app(folder).
- `cd my-app`, para entrarmos no diretório do novo app.
- `npm start`, inicia o servidor de desenvolvimento.

#### Docs:
- [Create a New React App](https://reactjs.org/docs/create-a-new-react-app.html)
- [Create React App Github](https://github.com/facebook/create-react-app)


### Hello World

Veja um exemplo básico de renderização em React:

``` js
ReactDOM.render(
    <h1>Hello, world!</h1>,
    document.getElementById('root')
);
```

O primeiro parâmtro da função `render` é o elemento que queremos renderizar. O segundo parâmetro é o elemento que receberá o elemento a ser renderizado. Simples não?

### JSX

O React usa objetos de JavaScript para criar seus elementos. Usaremos esses elementos de React para descrever como será a interface da página, e o React será responsável por gerar os nós DOM para alcançar o resultado.

Exemplo:

```const element = <h1>Hello, world!</h1>;```

Esta sintaxe estranha de tags não é uma string, nem HTML.

É chamada JSX e é uma extensão de sintaxe para JavaScript. Recomendamos usar JSX com o React para descrever como a UI deveria parecer. JSX pode lembrar uma linguagem de template, mas que vem com todo o poder do JavaScript.

JSX produz “elementos” do React.

O React adota o fato de que a lógica de renderização é inerentemente acoplada com outras lógicas de UI: como eventos são manipulados, como o state muda com o tempo e como os dados são preparados para exibição.

Ao invés de separar tecnologias artificialmente colocando markup e lógica em arquivos separados, o React separa conceitos com unidades pouco acopladas chamadas “componentes” que contém ambos.

O React não requer o uso do JSX. Porém, a maioria das pessoas acha prático como uma ajuda visual quando se está trabalhando com uma UI dentro do código em JavaScript. Ele permite ao React mostrar mensagens mais úteis de erro e aviso.

Exemplo sem JSX:

``` js
const element = React.createElement(
    'div',
    null,
    'Hello, world!'
);
```
``` js
ReactDOM.render(
    element,
    document.getElementById('root')
);
```

 O método `.createElement()` possui a seguinte assinatura:e:

```React.createElement( /* type */, /* props */, /* content */ );```

Vamos dividir para saber o que é cada item:

- type – uma string ou um componente React. Pode ser uma string de qualquer elemento HTML existente (e.g. 'p', 'span', ou 'header'), ou você pode passar um componente React (criaremos componentes com JSX em breve).

- props – null ou um objeto. Este é um objeto de atributos HTML e dados personalizados sobre o elemento.

- content – null, uma string, um elemento de React ou um componente de React. Qualquer coisa que você passar aqui será o conteúdo do elemento a ser renderizado. Pode ser um texto simples, código JavaScript, outros elementos de React, etc.

Agora o mesmo exemplo com JSX:

``` js
class HelloWord extends Component {
    render() {
        return <div>Hello, world!</div>
    }
}
```

Mais adiante veremos mais sobre classes. Veja que fica muito mais simples com JSX, porém, ainda assim continua sendo um objeto, apenas com um sintaxe um pouco diferente. JSX NÃO É HTML! O React é o responsável pela renderização a partir de objetos.

### Render 

Suponhamos que exista um `<div>` em algum lugar do seu código HTML:

``` <div id="root"></div> ```

Nós o chamamos de nó raiz do DOM porque tudo dentro dele será gerenciado pelo React DOM.

Aplicações construídas apenas com React geralmente tem apenas um único nó raiz no DOM. Se deseja integrar o React a uma aplicação existente, você pode ter quantos nós raiz precisar.

Para renderizar um elemento React em um nó raiz, passe ambos para `ReactDOM.render()`:

``` ReactDOM.render(<h1>Hello, world</h1>, document.getElementById('root')); ```

Elementos React são imutáveis. Uma vez criados, você não pode alterar seus elementos filhos ou atributos.

Com o que aprendemos até agora, a única forma de atualizar a interface é criar um novo elemento e passá-lo para `ReactDOM.render()`.

O React DOM compara o elemento novo e seus filhos com os anteriores e somente aplica as modificações necessárias no DOM para levá-lo ao estado desejado.

#### Docs:
- [React - Getting Started](https://reactjs.org/docs/)


## Desafio

Criar um App que exiba a frase `Hello World!` em 10 formas diferentes. Além disso, exiba a data atual em cada uma das formas, para isso será necessário o [JavaScript Date Objects](https://www.w3schools.com/js/js_dates.asp).

Seja criativo, por exemplo, use letras maiúsculas, minúsculas, de trás pra frente e etc.

- De um [Fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) no repositório: [https://gitlab.com/acct.fateclab/turma-2-sem-2019/01-introducao-ao-react](https://gitlab.com/acct.fateclab/turma-2-sem-2019/01-introducao-ao-react);
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com **HTTPS**. 
- Agora localmente abra uma pasta e use o botão direito do Mouse para abrir o "Git Bash", com esse atalho você chegará na pasta que quer mais rapidamente pelo terminal.
- Use o comando `git clone url-copiada-do-gitlab` para que a estrutura de pastas do repositório seja clonada na sua pasta
- Ainda no terminal e execute o comando `npm create-react-app introducao-ao-react`, isso criará as pastas e arquivos bases do React;
- Com as pastas base criadas, reescreva o arquivo `App.js` para que tenha até 10 textos "Hello World" diferentes. Poderá ser utilizado CSS ou métodos Javascript que alterem o texto;
- Para rodar não se esqueça de usar `npm run start` para que o App continue sendo buildado sempre que atualizar os arquivos;
- Faça [commits](https://git-scm.com/docs/git-commit) para cada forma diferente de exibição do texto;
- Assim que terminar dê `git push origin seu-nome/introducao-ao-react`, crie um Merge Request na interface do GitLab, acesse o menu "Merge Requests" e crie um, configure o "Target Branch" para o [repositório original](https://gitlab.com/acct.fateclab/turma-2-sem-2019/01-introducao-ao-react) para que seu App seja avaliado e revisado e para que possamos te dar um feedback.
- O nome do Merge Request deve ser o seu nome completo.

Para se destacar na execução:
- Crie uma branch `seu-nome/introducao-ao-react` no seu repositório;
- Personalize a página com CSS;

Veja alguns métódos para arrays e loops que podem ajudar:

- [for of](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Statements/for...of)
- [forEach](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)
- [map](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/map)


## Extras:

- [NPM](https://docs.npmjs.com/)
- [Como fazer um README](https://medium.com/@raullesteves/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8)